
 "uses strict";
 function createFormElements(){
     let formRow = document.querySelector("#rowcount");
     formRow.addEventListener("blur",generateTable);
     let formColumn = document.querySelector("#colcount");
     formColumn.addEventListener("blur",generateTable);
     let formWidth = document.querySelector("#tablewidth");
     formWidth.addEventListener("blur",generateTable);
     let formTextColor = document.querySelector("#textcolor");
     formTextColor.addEventListener("blur",generateTable);
     let formBackgroundColor = document.querySelector("#backgroundcolor");
     formBackgroundColor.addEventListener("blur",generateTable);
     let formBorderWidth = document.querySelector("#borderwidth");
     formBorderWidth.addEventListener("blur",generateTable);
     let formBorderColor = document.querySelector("#bordercolor");
     formBorderColor.addEventListener("blur",generateTable);
 }

 function generateTable(){
     /**This section checks if there is already a table. If there is, the code will 
      * remove it in order for 1 table to exist at all times*/
     let check = document.querySelector("table");
     if( check !== null){
        let toRemove = document.querySelector("table"); 
        toRemove.remove();
     }
     /**This section creates the table*/
     let newTable = document.createElement("table");
     document.querySelector("#table-render-space").insertBefore(newTable,null);
     let counter = 0;
     for(let i = 0; i < document.querySelector("input").value; i++){
     let newTableRow = document.createElement("tr");
     document.querySelector("table").insertBefore(newTableRow,null);
     for(let j = 0; j < document.querySelectorAll("input")[1].value; j++){
     let newTableCell = document.createElement("td");
     document.querySelectorAll("tr")[i].insertBefore(newTableCell,null);
     document.querySelectorAll("td")[counter].innerHTML = `cell${i}${j}`;
     counter++;
        }
    }
    /**Widths*/
    let newWidth = document.querySelector("#tablewidth").value;
    document.querySelector("table").style.width = `${newWidth}%`;
    let borderWidth = document.querySelector("#borderwidth").value;
    document.querySelector("table").style.borderWidth =  `${borderWidth}px`;
    for(let h = 0; h < document.querySelectorAll("td").length; h++){
    document.querySelectorAll("td")[h].style.borderWidth = `${borderWidth}px`;
    }
    /**Color section*/
    for(let h = 0; h < document.querySelectorAll("td").length; h++){
        document.querySelectorAll("td")[h].style.color = document.querySelector("#textcolor").value;
    }
    for(let h = 0; h < document.querySelectorAll("td").length; h++){
        document.querySelectorAll("td")[h].style.borderColor = document.querySelector("#bordercolor").value;
    }
    document.querySelector("table").style.borderColor = document.querySelector("#bordercolor").value;
    document.querySelector("table").style.backgroundColor = document.querySelector("#backgroundcolor").value;
    /**This section creates the html code in the textarea*/
    let text = "<table>";
    for(let i = 0; i < document.querySelector("input").value; i++){
        text+="\r\t<tr>";
        for(let j = 0; j < document.querySelectorAll("input")[1].value; j++){
            text+=`\r\t\t<td>cell${i}${j}</td>`;
        }
        text+="\r\t</tr>";
 }
 text+="\r</table>";
    document.querySelector("textarea").textContent = text;
}

 document.addEventListener("DOMContentLoaded",createFormElements);
 