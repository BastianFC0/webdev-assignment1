/**
 * @param {string} elem - an HTML tag
 * @param {string} color - A color name
 * @param {string} width - A percentage for the , no need to include the "%" symbol
 */

`uses strict`;
function changeTagTextColor(elem,color){
     for (let i=0;i<document.getElementsByTagName(`${elem}`).length;i++){
        document.getElementsByTagName(`${elem}`)[i].style.color=`${color}`;
     }
}

function changeTagBackground(elem,color){
    for (let i=0;i<document.getElementsByTagName(`${elem}`).length;i++){
       document.getElementsByTagName(`${elem}`)[i].style.backgroundColor=`${color}`;
    }
}

function changeTagWidth(elem,width){
    for (let i=0;i<document.getElementsByTagName(`${elem}`).length;i++){
       document.getElementsByTagName(`${elem}`)[i].style.width=`${width}%`;
    }
}

function changeTagBorderColor(elem,color){
    for (let i=0;i<document.getElementsByTagName(`${elem}`).length;i++){
       document.getElementsByTagName(`${elem}`)[i].style.borderColor=`${color}`;
    }
}